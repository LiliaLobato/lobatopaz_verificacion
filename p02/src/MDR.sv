// =============================================================================
// Title        : Multiplicador, Divisor y Raiz cuadrada
// Project      :   p02
// File         :   MDR.sv
// Date         :   21/04/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

import mult_top_pkg::*;

module MDR #(
parameter DW = 4
) (
	input               clk,
	input               rst,
	input               pipo_en,
	input               done_en,
	input               load_en,
	input  [WIDTH_CNT-1:0]  	i_count,
	input  [DW-1:0]		x_bin,
	input  [DW-1:0]		y_bin, 
	input  op_e			op,
	output [DW*2:0]     out,
	output [DW-1:0]		r_sqrt, 
	output              sign_mult
);

logic [DW-1:0]	sum;
logic [DW-1:0]	sum_pipo;
logic [DW-1:0]	sum_shifted;
logic [DW-1:0]	r_sum;
logic [DW-1:0]	r_sqrt_wr;
logic [DW-1:0]	sum_wr;
logic [DW-1:0]	sqrt_sum_0;
logic [DW-1:0]	sqrt_sum_1;
logic [DW*2:0]	out_next_R;
logic [DW*2:0]	out_sel;
logic [DW*2:0]	out_next_L;
logic [DW-1:0]	x_bin_shifted;
logic [DW-1:0]	x_c2;
logic [DW-1:0]	y_c2;
logic [DW*2:0]	n_product;
logic [DW*2:0]	out_wr;
logic [DW*2:0]	out_shifted;
logic [DW-1:0]	out_sum_wr;

//shifter 
pipo_shift #(
.DW(DW*2)
) shift_adder(
	.clk(clk),
	.rst(rst),
	.enb(pipo_en),
	.inp(n_product),
	.out(out_next_R)
); 

pipo_shiftL #(
.DW(DW*2)
) shift_adder_L(
	.clk(clk),
	.rst(rst),
	.enb(pipo_en),
	.inp(n_product),
	.out(out_next_L)
); 

pipo_shiftL_N #(
.DW(DW*2),
.N(2)
) shif_out_N(
	.clk(clk),
	.rst(rst),
	.enb(pipo_en),
	.inp(out),
	.out(out_shifted)
); 

pipo_shiftL_N #(
.DW(DW),
.N(2)
) shift_sum_N(
	.clk(clk),
	.rst(rst),
	.enb(pipo_en),
	.inp(sum),
	.out(sum_shifted)
); 

pipo_Nenb #(
.DW(IN_DW)
) r_sum_pipo(
	.clk(clk),
	.rst(rst),
	.inp(sum), 
	.out(r_sum)
);

C2 #(
.DW(DW)
) c2_y(
	.binario      (y_bin),
	.complemento2 (y_c2)
);

C2 #(
.DW(DW)
) c2_x(
	.binario      (x_bin),
	.complemento2 (x_c2)
);

C2 #(
.DW(DW)
) c2_sqrt_sum_0(
	.binario      (out_shifted | 1'b1),
	.complemento2 (sqrt_sum_0)
);

assign sqrt_sum_1 = (out_shifted | 2'b11);
assign x_bin_shifted = (x_bin >> i_count);

//SUMADOR
always_comb begin
	case (op)
		MULTIPLICATION : begin
			case(out[1:0])
			    2'b00, 2'b11 : begin
			        sum_wr = '0;
			    end
			    2'b10 : begin
			        sum_wr = y_c2;
			    end
			    2'b01 : begin
			        sum_wr = y_bin;
			    end
			    default : begin
			        sum_wr = '0;
			    end
			endcase
			out_sum_wr = out[(DW*2):DW+1];
		end
		DIVITION : begin
			sum_wr = y_c2;
			out_sum_wr = out[(DW*2):DW+1];
		end
		SQUARE : begin
			case( r_sum[DW-1] )
			    1'b0 : begin 
			        sum_wr = pipo_en?sqrt_sum_0:'0;
			    end
			    1'b1 : begin
			        sum_wr = pipo_en?sqrt_sum_1:'0;
			    end
			    default : begin
			        sum_wr = '0;
			    end
			endcase
			out_sum_wr = ( sum_pipo << 2 |  ( (x_bin_shifted) & 3) );
		end
		default : begin
		    sum_wr = '0;
			out_sum_wr = '0;
	    end
	endcase
	sum = done_en?'0:(out_sum_wr + sum_wr);
end 

pipo_Nenb #(
.DW(DW)
) sum_reg (
	.clk(clk),
	.rst(rst),
	.inp(sum), 
	.out(sum_pipo)
);


//RETROALIMENTACIÓN AL SHIFTER
always_comb begin
	case (op)
		MULTIPLICATION : begin
			n_product = done_en?out:(pipo_en?{sum,out[DW:0]}:{{DW{1'b0}},x_bin,1'b0});
			out_sel = done_en?out_wr:out_next_R; 
		end
		DIVITION : begin
			n_product = done_en?out:(pipo_en?(sum[DW-1]?out:{sum,out[DW:0]}):{{DW-1{1'b0}},x_bin,2'b0});
			out_sel = done_en?out_wr:{out_next_L[DW*2:1],!sum[DW-1]};
		end
		SQUARE : begin
			n_product = done_en?out:(pipo_en?{{DW-1{1'b0}},out[DW:0]}:'0);
			out_sel = done_en?out_wr:(pipo_en?(out_next_L | !sum[DW-1] ):'0);
		end
		default : begin
			n_product = '0;
			out_sel = '0;
	    end
	endcase
end 

assign out_wr =  out; 
assign out = out_sel; 
assign sign_mult = x_c2==x_bin?1'b1:1'b0;

assign r_sqrt_wr = r_sqrt;  
assign r_sqrt = done_en?r_sqrt_wr:r_sum;


endmodule
