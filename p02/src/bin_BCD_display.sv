// =============================================================================
// Title       	:	Implementación de un display de 7 segmentos a DECIMAL
// Inputs		:	4 bits decimal number 
// Outputs		:	3 displays de 7 segmentos
// Project     	: 	t01
// File        	: 	bin_BCD_display.sv
// Date 		: 	28/01/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module bin_BCD_display
import bin_BCD_pkg::*;
(
	input  dataDec_t	dec_num,
	output segment_t	seg_num   
);

always_comb begin
									//abcdefg
		seg_num =  	(dec_num == 4'd0) ? (7'b0000001): //0  
					(dec_num == 4'd1) ? (7'b1001111): //1
					(dec_num == 4'd2) ? (7'b0010010): //2
					(dec_num == 4'd3) ? (7'b0000110): //3
					(dec_num == 4'd4) ? (7'b1001100): //4
					(dec_num == 4'd5) ? (7'b0100100): //5
					(dec_num == 4'd6) ? (7'b0100000): //6
					(dec_num == 4'd7) ? (7'b0001111): //7
					(dec_num == 4'd8) ? (7'b0000000): //8
					(dec_num == 4'd9) ? (7'b0000100): //9
										(7'b0000000); //NADA
		
end

endmodule

