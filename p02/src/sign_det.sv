// =============================================================================
// Title       	:	Implementacion de un detector de signo para el MDR
// Project     	: 	p02
// File        	: 	sign_det.sv
// Date 		: 	01/04/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
//					Gabriel Paz	   ie707023
// =============================================================================


module sign_det
import mult_top_pkg::*;
(
	input  logic	y_sign,
	input  logic	x_sign,
	input  logic	rst,
	input  logic	sign_mult,
	output logic 	sign,
	output logic 	sign_rem,
	input  op_e		data_op
);

logic sign_wr;
logic sign_sel;

assign sign_wr = rst? (y_sign ^ x_sign):'0 ;

always_comb begin
case(data_op)
    MULTIPLICATION: begin
        sign_sel = sign_mult ^ sign_wr;
    end
    DIVITION: begin
        sign_sel = sign_wr;
    end
    default: begin
        sign_sel = '0;
    end
endcase 
end 

assign sign_rem = rst? (x_sign | y_sign) & x_sign:'0 ;
assign sign = sign_sel;



endmodule