// =============================================================================
// Title        : Shifter
//                  The parallel input will be the parallel output shifted 1
// Inputs       : Parallel inp
// Outputs      : Parallel output
// Project      :   t06
// File         :   shifter.sv
// Date         :   08/03/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module shifter 
import bin_BCD_pkg::*;
(
input  dataDD_t     inp,
output dataDD_t     out
);

assign out  = inp << 1;

endmodule

