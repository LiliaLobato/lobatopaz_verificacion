onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {UART CLK}
add wave -noupdate /tb_uart/uut/clk
add wave -noupdate /tb_uart/uut/rst
add wave -noupdate -divider {UART System}
add wave -noupdate -color Violet /tb_uart/uut/serial_data_rx
add wave -noupdate -color Violet /tb_uart/uut/rx_interrupt
add wave -noupdate -color Violet -radix hexadecimal /tb_uart/uut/parallel_data_rx
add wave -noupdate /tb_uart/uut/clear_interrupt
add wave -noupdate /tb_uart/uut/clk_uart_tx/clk_gen
add wave -noupdate -color {Slate Blue} -radix hexadecimal /tb_uart/uut/parallel_data_tx
add wave -noupdate -color {Slate Blue} /tb_uart/uut/transmit
add wave -noupdate -color {Slate Blue} /tb_uart/uut/serial_data_tx
add wave -noupdate -divider {RX UART}
add wave -noupdate /tb_uart/uut/uart/rx_uart/fsm_rx/edo_actual
add wave -noupdate /tb_uart/uut/uart/rx_uart/data
add wave -noupdate /tb_uart/uut/uart/rx_uart/clear
add wave -noupdate -radix hexadecimal /tb_uart/uut/uart/rx_uart/data_parallel
add wave -noupdate /tb_uart/uut/uart/rx_uart/RX_interrupt
add wave -noupdate /tb_uart/uut/uart/rx_uart/parity_error
add wave -noupdate -divider {TX UART}
add wave -noupdate /tb_uart/uut/uart/tx_uart/fsm_tx/edo_actual
add wave -noupdate /tb_uart/uut/uart/tx_uart/transmit
add wave -noupdate -radix hexadecimal /tb_uart/uut/uart/tx_uart/data
add wave -noupdate /tb_uart/uut/uart/tx_uart/serial_output
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 3} {5050000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 284
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {4783713 ps} {6662897 ps}
