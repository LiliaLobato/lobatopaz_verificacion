import top_uart_pkg::*;

module proc_to_out (
	input		clk,
	input 		rst,
	input		clean_acc,			//ESTA BANDERA LIMPIA LOS CONTADORES
	input		cnt_Bselector_enb,	//ENCIENDE ESTE CUANDO EMPIECE EL FLUSH
	input 		second_run, 		//SELECTOR PARA FIFOS[0-4] O FIFOS[5-8]
	input result_t  result_acc_0;
	input result_t  result_acc_1;
	input result_t  result_acc_2;
	input result_t  result_acc_3;
	input result_t  result_acc_4;
	input result_t  result_acc_5;
	input result_t  result_acc_6;
	input result_t  result_acc_7;
	input uart_baud_t  fifo_num, //NUMERO DE RESULTADOS

    output uart_baud_t  result
);

byteF_sel cnt_Bselector_count;

case (second_run)
	1: begin
		result_a = result_acc_0;
		result_b = result_acc_1;
		result_c = result_acc_2;
		result_d = result_acc_3;
	end
	default: begin
		result_a = result_acc_4;
		result_b = result_acc_5;
		result_c = result_acc_6;
		result_d = result_acc_7;
	end
endcase

//Flush for FIFOS
localparam FIFO_W = 3;
typedef enum logic [FIFO_W-1:0]
{
	FIFO_0	= 3'b000,
	FIFO_1	= 3'b001,
	FIFO_2	= 3'b010,
	FIFO_3	= 3'b011,
	FIFO_4	= 3'b100
}fifo_sel;

//BYTE flush
localparam BYTEF_W = 2;
typedef enum logic [BYTEF_W-1:0]
{
	BF0_SEL	= 2'b00,
	BF1_SEL	= 2'b01,
	BF2_SEL	= 2'b10
}byteF_sel;

//cnt for FIFO flush
dinamMAX_counter_ovf #(
	.DW(DW_PC)
) cnt_fifo_flush (
	.clk(clk),
	.rst(rst),
	.enb(cnt_Bselector_ovf),
	.clean(clean_acc),
	.max_ovf(fifo_num),
	.count (fifo_flush_count),
	.ovf(cnt_fifo_flush_ovf)
);

//seletcor of byte
bin_counter_ovf_clean #(
	.DW(DW_RES),
	.MAXCNT(CNT_BYTE)
) cnt_Bselector (
	.clk(clk),
	.rst(rst),
	.enb(cnt_Bselector_enb),
	.clean(clean_acc),
	.count(cnt_byte_sel),
	.ovf(cnt_Bselector_ovf)
);

assign cnt_fifo_flush_count = fifo_sel'(fifo_flush_count);
assign cnt_Bselector_count = (cnt_byte_sel <= 1)? (BF0_SEL): (cnt_byte_sel <= 3)? (BF1_SEL): (BF2_SEL);


/** MUX to select FIFO to flush*/
always_comb begin
    case (cnt_fifo_flush_count)
        FIFO_0: begin
            result_flush_val = result_a;
        end
        FIFO_1: begin
            result_flush_val = result_b;
        end
        FIFO_2: begin
            result_flush_val = result_c;
        end
        default: begin
            result_flush_val = result_d;
        end
    endcase
end

//BYTES TO FLUSH
always_comb begin
    case (cnt_Bselector_count)
        BF0_SEL: begin
            result = result_flush_val[DW_PC-1 : 0];
        end
        BF1_SEL: begin
            result = result_flush_val[(DW_PC*2)-1 : DW_PC];
        end
        default: begin
            result = {4'b0, result_flush_val[DW_RES-1:(DW_PC*2)]};
        end
    endcase
end

endmodule proc_to_out