// =============================================================================
// Title        : One Shot
// Inputs       : System clk, reset, n_times, dbcrs_start
// Outputs      : enable
// Project      :   t05
// File         :   oneShot.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

import clk_div_pkg::*;

module oneShot_dbcr
(
	input  clk,
	input  rst,
  input  in,
	output out
);

//Debouncer y clk_gen
logic Delay30ms_ready; 
logic EnableCounter;

fsm_dbcr i_fsm_dbcr (
    .clk            (clk),
    .rst_n          (rst),
    .Din            (in),
    .Delay30ms_ready(Delay30ms_ready),
    .EnableCounter  (EnableCounter),
    .one_shot       (out)
);

cntr_mod_n_ovf #(
  .FREQ(REFERENCE_CLKFPGA/2),
  .DLY(0.03)
) i_cntr_mod_n (
    .clk    (clk),
    .rst    (rst),
    .enb    (EnableCounter),
    .ovf    (Delay30ms_ready),
    .count  ()
);

endmodule


