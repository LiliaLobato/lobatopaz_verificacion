// =============================================================================
// Title        : Pcomparator
//                  compares two busses and gets flag = 1 if equal
// Project      :   t09
// File         :   pipo.sv
// Date         :   05/05/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module comparator #(
parameter DW = 1
) (
input               clk,
input               rst,
input               enb,
input  [DW-1:0]     in_A,
input  [DW-1:0]     in_B,
output 				out
);

logic [DW-1:0]      rgstr_r     ;

always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
        rgstr_r  <= '0;
    else if (enb)
        rgstr_r  <= (in_A==in_B)?'1:'0;
end:rgstr_label

assign out  = rgstr_r;

endmodule

