// =============================================================================
// Title        : Anti_OneShot
//                  to work on both clk
// Project      :   t09
// File         :   fsm_tx.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

import top_uart_pkg::*;
module cnt_block_mxv (
	input		clk,
	input 		rst,
	input		clean_acc,
	input		cnt_fifo_pop_enb,
	input		cnt_Bselector_enb,
	input uart_baud_t cnt_fifo_pop_MAXovf,
	input fifo_sel cnt_fifo_flush_MAXovf,
	output logic cnt_fifo_pop_ovf,
	output logic cnt_fifo_flush_ovf,
	output fifo_sel cnt_fifo_flush_count,
	output byteF_sel cnt_Bselector_count
);


uart_baud_t  fifo_flush_count;
uart_baud_t  fifo_flush_MAXovf;
result_t cnt_byte_sel;
logic	cnt_Bselector_ovf;

//cnt for FIFO POPs
dinamMAX_counter_ovf #(
	.DW(DW_PC)
) cnt_fifo_pop (
	.clk(clk),
	.rst(rst),
	.enb(cnt_fifo_pop_enb),
	.max_ovf(cnt_fifo_pop_MAXovf),
	.count (),
	.clean(clean_acc),
	.ovf(cnt_fifo_pop_ovf)
);

//cnt for FIFO flush
dinamMAX_counter_ovf #(
	.DW(DW_PC)
) cnt_fifo_flush (
	.clk(clk),
	.rst(rst),
	.enb(cnt_Bselector_ovf),
	.max_ovf(fifo_flush_MAXovf),
	.count (fifo_flush_count),
	.clean(clean_acc),
	.ovf(cnt_fifo_flush_ovf)
);

//seletcor of byte
bin_counter_ovf_clean #(
	.DW(DW_RES),
	.MAXCNT(CNT_BYTE)
) cnt_Bselector (
	.clk(clk),
	.rst(rst),
	.enb(cnt_Bselector_enb),
	.clean(clean_acc),
	.ovf(cnt_Bselector_ovf),
	.count(cnt_byte_sel) 
);

assign fifo_flush_MAXovf = uart_baud_t'(cnt_fifo_flush_MAXovf)+1'b1;
assign cnt_fifo_flush_count = fifo_sel'(fifo_flush_count);
assign cnt_Bselector_count = (cnt_byte_sel <= 1)? (BF0_SEL): (cnt_byte_sel <= 3)? (BF1_SEL): (BF2_SEL);

endmodule