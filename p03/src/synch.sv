// =============================================================================
// Title        : Anti_OneShot
//                  to work on both clk
// Project      :   t09
// File         :   fsm_tx.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module synch
import top_uart_pkg::*;
(
	input		clk,
	input 		rst,
	input		n_synch,
	output logic synch
);

logic signal_reg;

//synch on register
pipo_Nenb reg_synch(
	.clk(clk),
	.rst(rst),
	.inp(n_synch),
	.out(signal_reg)
);

//Change in clk
oneShot oneShot_synch (
	.clk(clk),
	.rst(rst),
	.inp(signal_reg),
	.out(synch)
);

endmodule