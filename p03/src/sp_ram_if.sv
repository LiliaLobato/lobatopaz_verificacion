/*
Coder:          Abisai Ramirez Perez
Date:           03/31/2019
Name:           sp_rami_if.sv
Description:    This is the interface of a single port random access memory. 
*/
`ifndef SP_RAM_IF_SV
    `define SP_RAM_IF_SV

interface sp_ram_if ();
import sp_ram_pkg::*;

// Write enable signal
logic       we          ;   // Write enable
data_t      data        ;   // data to be stored
data_t      rd_data     ;   // read data from memory
addr_t      rw_addr     ;   // Read write address

modport mem (
input   we,
input   data,
input   rw_addr,
output  rd_data
);

modport cln (
output  we,
output  data,
output  rw_addr,
input   rd_data
);

endinterface
`endif

