// =============================================================================
// Title        : Pipo with clean flag
//                  The parallel input will be the parallel output 
// Inputs       : System clk, reset, enb, parallel input
// Outputs      : Parallel output
// Project      :   t09
// File         :   pipo.sv
// Date         :   05/05/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module holder #(
parameter DW = 1
) (
input               clk,
input               rst,
input               enb,
input  [DW-1:0]     inp,
output [DW-1:0]     out,
input 				clean
);

logic [DW-1:0]      rgstr_r     ;
logic enb_clean;

always_ff@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
        rgstr_r  <= '0;
    else if (enb_clean)
        rgstr_r  <= enb?inp:clean?'0:'0;
end:rgstr_label

assign out = rgstr_r;
assign enb_clean = enb | clean;

endmodule

