// =============================================================================
// Title        : Anti_OneShot
//                  to work on both clk
// Project      :   t09
// File         :   fsm_tx.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module anti_oneShot
import top_uart_pkg::*;
(
	input		clk,
	input 		rst,
	input		inp,
	output logic out
);

aos_state_e edo_actual;	
aos_state_e edo_siguiente;

//asignaciones por default y siguiente
always_ff@(posedge clk or negedge rst) begin // Circuito Secuenicial en un proceso always.
   if (!rst) 
      edo_actual  <= IDLE_AS;
   else 
      edo_actual  <= edo_siguiente;
end

// Circuito combinacional entrada, define ssiguiente estado
always_comb begin
	case(edo_actual)
		IDLE_AS: begin //RX INITS
			edo_siguiente <= inp?CYCLE_1_AS:IDLE_AS;
		end
		CYCLE_1_AS: begin
			edo_siguiente <= CYCLE_2_AS;
		end
		CYCLE_2_AS: begin
			edo_siguiente <= CYCLE_3_AS;
		end
		CYCLE_3_AS: begin
			edo_siguiente <= CYCLE_4_AS;
		end
		CYCLE_4_AS: begin
			edo_siguiente <= IDLE_AS;
		end
		default: begin
			edo_siguiente <= IDLE_AS;
		end
	endcase
end

always_comb begin
	case(edo_actual)
		IDLE_AS: begin
			out = OFF;
		end
		CYCLE_1_AS: begin
			out = ON;
		end
		CYCLE_2_AS: begin
			out = ON;
		end
		CYCLE_3_AS: begin
			out = ON;
		end
		CYCLE_4_AS: begin
			out = ON;
		end
		default: begin
			out = OFF;
		end
	endcase
end

	

endmodule