// =============================================================================
// Title        :   FIFO
// Inputs       :   4 bits decimal number 
// Outputs      :   3 displays de 7 segmentos
// Project      :   t08
// File         :   fifo.sv
// Date         :   21/04/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================
//Description: this is the module of FIFO, save a data into a memory. Output empty is high when the fifo is empty and output full is high when fifo is full.
//Write enable makes push and read enable makes pops. 

import sdp_sc_ram_pkg::*;

module fifo (
    input  logic  clk,    //clk
    input  logic  rst,    //rst
    input  logic  push,   //push enable
    input  logic  pop,    // pop enable
    input  logic  clean,  // pop enable
    input  data_t DataInput,    //data input
    output data_t DataOutput,   //data output
    output logic  empty,  // signal of empty
    output logic  full    //signal of full
  );

logic           en_out;    //fsm out control
sdp_sc_ram_if   mem_if (); //interface
data_t          fifo_data; //hold data out
    
//RAM memory instance
sdp_sc_ram ram (
  .clk(clk),
  .data(DataInput),
  .mem_if(mem_if.mem)
); 

// FIFO control
fsm_fifo fsm_fifo (
  .clk(clk),
  .rst(rst),
  .pop(pop),
  .push(push),
  .clean(clean),
  .en_out(en_out),
  .mem_if(mem_if.cln_fu),
  .full(full),
  .empty(empty)
);

// FIFO output
always_ff@(posedge clk, negedge rst) begin
  if(!rst) begin
    fifo_data <= '0;
  end else if(en_out) begin
    fifo_data <= clean?'0:mem_if.rd_data;
  end
  
end

assign DataOutput = fifo_data;

endmodule: fifo// --------------------------------------------------------------------------

