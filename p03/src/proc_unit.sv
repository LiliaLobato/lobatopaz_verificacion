// =============================================================================
// Title        : Pipo
//                  The parallel input will be the parallel output 
// Inputs       : System clk, reset, parallel input
// Outputs      : Parallel output
// Project      :   t05
// File         :   pipo_Nenb.sv
// Date         :   21/02/2020
// =============================================================================
// Authors      :   Lilia Lobato   ie706937
//                  Gabriel Paz    ie707023
// =============================================================================

module proc_unit
import top_uart_pkg::*;
(
input uart_baud_t  in_a,
input uart_baud_t  in_b,
input result_t  in_acc,
output result_t out
);

	assign	out = (in_a * in_b) + in_acc;

endmodule