
import top_uart_pkg::*;
module counter_uart_rx
(
	input    		clk,
	input   		rst,
	input  			enb,
	output logic 	ovf
);


logic [CNT_DW_PC:0]	 count_r;
logic [CNT_DW_PC:0]  count_val;
logic	 first;
assign	count_val = (first)? (DW_PC + 1'd1) : (DW_PC);


always_ff@(posedge clk or negedge rst) begin
	if(!rst) begin
		count_r <= '0;
		first   <= ON;
	end
		
	else if(enb) begin
		if(count_r < count_val)
			count_r <= count_r + 1'd1;
		else begin
			count_r <= '0;
			first   <= OFF;
		end
	end
end 

always_comb  begin
   if (count_r == (count_val - 1'd1))
       ovf     =   ON;    
   else
       ovf     =   OFF;
end 



endmodule
