// =============================================================================
// Title       	:	Implementación de un decodificador binario en sistema complemento a 2
//					a su equivalente en unidades, decenas y centenas
// Inputs		:	8 Switches
// Outputs		:	3 displays de 7 segmentos
// Project     	: 	t01
// File        	: 	binC2_BCD_top.sv
// Date 		: 	28/01/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
// =============================================================================
	// localparam BIT_IN = 8;
	// localparam SEG_OUT = 7;

	// typedef logic [BIT_IN-1:0]		dataIn_t;
	// typedef logic [(BIT_IN>>1)-1:0]	dataDec_t;
	// typedef logic [SEG_OUT-1:0] 	segment_t;

module binC2_BCD_top
import binC2_BCD_pkg::*;
(
	// SW
	input	dataIn_t BinIn,
	// 7 SEG
	output	segment_t cen_seg,
	output	segment_t dec_seg,
	output	segment_t uni_seg,
	// LEDG[0]
	output  logic 		sign_led
);

// Wires
dataDec_t cen_wire;
dataDec_t dec_wire;
dataDec_t uni_wire;

//Conversion de binario a BCD
binC2_BCD_convert bin_to_BCD
(
	.num_binA2(BinIn),
	.sign(sign_led),
	.dec_cen(cen_wire),
	.dec_dec(dec_wire),
	.dec_uni(uni_wire)
);

//Display para centenas
binC2_BCD_display cen_segment
(
	.dec_num(cen_wire),
	.seg_num(cen_seg)   
);

//Display para decenas
binC2_BCD_display dec_segment
(
	.dec_num(dec_wire),
	.seg_num(dec_seg)  
);

//Display para unidades
binC2_BCD_display uni_segment
(
	.dec_num(uni_wire),
	.seg_num(uni_seg)   
);

endmodule