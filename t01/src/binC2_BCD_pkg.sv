// =============================================================================
// Title       	:	Package
// Project     	: 	t01
// File        	: 	binC2_BCD_pkg.sv
// Date 		: 	28/01/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
// =============================================================================

`ifndef BINC2_BCD_PKG_SV
`define BINC2_BCD_PKG_SV

package binC2_BCD_pkg;

	localparam BIT_IN = 8;
	localparam SEG_OUT = 7;
	localparam BIT_IN_H = 4;

	typedef logic [BIT_IN-1:0]		dataIn_t;
	typedef logic [BIT_IN_H-1:0]	dataDec_t;
	typedef logic [SEG_OUT-1:0] 	segment_t;

endpackage

`endif