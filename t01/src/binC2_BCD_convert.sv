// =============================================================================
// Title       	:	Implementación de un decodificador de binario en complemento  
//					a2 a decimal
// Inputs		:	8 bits binary number in complement 2  
// Outputs		:	Centenas, decenas, unidades y signo 
// Project     	: 	t01
// File        	: 	binC2_BCD_convert.sv
// Date 		: 	28/01/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
// =============================================================================

	// localparam BIT_IN = 8;
	// localparam SEG_OUT = 7;

	// typedef logic [BIT_IN-1:0]		dataIn_t;
	// typedef logic [(BIT_IN>>1)-1:0]	dataDec_t;
	// typedef logic [SEG_OUT-1:0] 	segment_t;

module binC2_BCD_convert
import binC2_BCD_pkg::*;
(
	input  	dataIn_t	num_binA2, 
	output 	sign,
	output 	dataDec_t  	dec_cen,
	output 	dataDec_t   dec_dec,
	output 	dataDec_t   dec_uni
);
	
	//num_bin viene en complemento a 2 por lo que se debe de pasar a no signado
	// si el último bit esta en 1 significa que es negativo
	dataIn_t num_bin;
	assign num_bin = (num_binA2[BIT_IN-1] == 1'b1) ? (~num_binA2 + 1'b1) : num_binA2;
	assign sign = num_binA2[BIT_IN-1];

	//Para calcular las centenas, dividimos entre 100
	//al no ser sintetizable la division: 
	//>> 12 is a division of 4096. When you want to divide by 100 you need to divide that by 4096/100 which is 40.9 or 41  
	assign dec_cen = ((num_bin * 'd41) >> 'd12);
	
	//Para calcular las decenas, restamos centenas y dividimos entre 10
	//>> 12 is a division of 4096. When you want to divide by 10 you need to divide that by 4096/10 which is 409.8 or 410  
	assign dec_dec = (((num_bin - (dec_cen * 'd100)) * 'd410) >> 'd12);


	//Para calcular las unidades, restamos decenas y centenas 
	assign dec_uni = num_bin - (dec_cen * 8'd100) - (dec_dec * 4'd10);

endmodule