onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -color Magenta /tb_binC2_BCD/sign_led
add wave -noupdate -radix decimal /tb_binC2_BCD/BinIn
add wave -noupdate -radix binary /tb_binC2_BCD/uni_seg
add wave -noupdate /tb_binC2_BCD/dec_seg
add wave -noupdate /tb_binC2_BCD/cen_seg
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {4096 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 180
configure wave -valuecolwidth 58
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {4096 ps} {14911 ps}
