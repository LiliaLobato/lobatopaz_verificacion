// =============================================================================
// Title       	:	Implementación de un decodificador binario en sistema complemento a 2
//					a su equivalente en unidades, decenas y centenas
// Inputs		:	8 Switches
// Outputs		:	3 displays de 7 segmentos
// Project     	: 	t01
// File        	: 	tb_binC2_BCD.sv
// Date 		: 	28/01/2020
// =============================================================================
// Authors     	: 	Lilia Lobato   ie706937
// =============================================================================
	// localparam BIT_IN = 8;
	// localparam SEG_OUT = 7;

	// typedef logic [BIT_IN-1:0]		dataIn_t;
	// typedef logic [(BIT_IN>>1)-1:0]	dataDec_t;
	// typedef logic [SEG_OUT-1:0] 	segment_t;

`timescale 1ns / 1ps
module tb_binC2_BCD();
import binC2_BCD_pkg::*;


// SW
dataIn_t 	BinIn;
// 7 SEG
segment_t 	cen_seg;
segment_t 	dec_seg;
segment_t 	uni_seg;
// LEDG[0]
logic		sign_led;

binC2_BCD_top uut (
	.BinIn 		(BinIn),
	.cen_seg 	(cen_seg),
	.dec_seg 	(dec_seg),
	.uni_seg 	(uni_seg),
	.sign_led 	(sign_led)
);

initial begin
        

        BinIn = 'd0;
        repeat (10)
        #1 BinIn = BinIn + 'd1;

        BinIn = 0'd0;
        repeat (10)
        #1 BinIn = BinIn + 'd10;

        #1 BinIn = 'd100;
    	#1 BinIn = 'd127;

        BinIn = 'd0;
        repeat (10)
        #1 BinIn = BinIn - 'd1;

        BinIn = 0'd0;
        repeat (10)
        #1 BinIn = BinIn - 'd10;

    	#1 BinIn = -'d127;

  
        #100
    $stop;
end

endmodule
